#import subprocess
import os
import popen2

yen_program = "/home/ubuntu/ripl/ripl/kshortestpaths/program"

def isInt(x):
    try:
	int(x)
	return True
    except ValueError:
	return False


class RouteFetcher:

    def __init__(self, graph_file, node_1, node_2, limit=0):
	self.file = graph_file
	self.src = node_1
	self.dst = node_2
	self.limit = limit

    def get_k_shortest(self):
	cmd_dict = {"program":yen_program, "file":self.file, 
		"src":self.src, "dst":self.dst, "limit":self.limit}
	cmd_str = "{program} '{file}' {src} {dst} {limit}"
	cmd_str = cmd_str.format(**cmd_dict)

	#print cmd_str
	#subprocess.call(cmd_str, shell=True)
	#os.system(cmd_str)
	child_stdout, child_stdin = popen2.popen2(cmd_str)
	#print "Child output:"

	paths = []
	for line in child_stdout:
	    paths.append(filter(isInt, line.split(' ')))

	return paths
	#subprocess.check_output([yen_program, self.file, self.src, self.dst])

if __name__=="__main__":
    graph_file = open("data/jelly_1")
    graph_data = graph_file.read()
    graph_file.close()
    
    graph = RouteFetcher(graph_data, 1, 2, 8)
    graph.get_k_shortest()
