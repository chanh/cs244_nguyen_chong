#!/usr/bin/env python
'''@package routing

Routing engine base class.

@author Brandon Heller (brandonh@stanford.edu)
'''
from copy import copy
from random import choice
from struct import pack
from zlib import crc32
from kshortestpaths.yenalgo import RouteFetcher

import logging
lg = logging.getLogger('ripl.routing')

DEBUG = False

lg.setLevel(logging.WARNING)
if DEBUG:
    lg.setLevel(logging.DEBUG)
    lg.addHandler(logging.StreamHandler())


class Routing(object):
    '''Base class for data center network routing.

    Routing engines must implement the get_route() method.
    '''

    def __init__(self, topo):
        '''Create Routing object.

        @param topo Topo object from Net parent
        '''
        self.topo = topo

    def get_route(self, src, dst):
        '''Return flow path.

        @param src source host
        @param dst destination host

        @return flow_path list of DPIDs to traverse (including hosts)
        '''
        raise NotImplementedError

class Callable:
    def __init__(self, anycallable):
        self.__call__ = anycallable


class JellyRouting(Routing):
    '''
    Jellyfish routing -- we cheat by using an algorithm known as Yen's K
    Shortest paths
    '''
    def __init__(self, topo):
        self.topo = topo
	self.graph_data = "" # done so we can lazy init this
	self.paths = {}
        self.route_list_count = [[0 for i in range((self.topo.n_))] \
                for i in range((self.topo.n_))]

    @staticmethod
    def is_switch(node_item):
        node_str = str(node_item)
        return (node_str[len(node_str)-1] == "0")

    @staticmethod
    def str_to_int_repr(node_item):
        '''Assumes format [\d]+_[\d]'''
        str_node = str(node_item)
        return int(str_node.split("_")[0])

    @staticmethod
    def int_to_str_repr(node_int):
        '''Converts {int} to {int}_0'''
        return str(node_int)+"_0"

    def setup_all_routes(self):
        '''Set up all routes before flows start'''
        hosts = self.topo.switches()
        for src_host in hosts:
            for dst_host in hosts:
                if (src_host == dst_host):
                    pass
                # print "Routing:", src_host, dst_host
                self.get_route(src_host, dst_host)

    def get_route(self, src, dst):
        #print "Debug: Inside JELLYFISH getroute"
        try:
          if (not self.graph_data):
            self.graph_data = self.topo.graph_data
        except AttributeError:
          #print "Not storing a global graph view"
          pass
	path_tuple = (str(src), str(dst))
        '''Remember this hack: taking the first significant digit as id'''
        #src_num = str(src)[0]
        #dst_num = str(dst)[0]
        src_num = JellyRouting.str_to_int_repr(src)
        dst_num = JellyRouting.str_to_int_repr(dst)
        if src_num == dst_num:
            return [str(src)]
	if (self.paths.get(path_tuple)):
            if (self.paths.get(path_tuple) == None):
                print "Error no paths found for route %i %i" % (src_num,
                        dst_num)
                return None
            self.route_list_count[src_num-1][dst_num-1]+=1;
            route_count = self.route_list_count[src_num-1][dst_num-1];
            return (self.paths.get(path_tuple))[route_count % len(path_tuple)]
	    #return choice(self.paths.get(path_tuple))
	else:
	    graph = RouteFetcher(self.graph_data, src_num, dst_num, 4)
            paths = graph.get_k_shortest()
            '''Shorthand for turning every switch "X" in each path into "X_0" '''
            paths = map(lambda path: map(lambda switch: '_'.join([switch,"0"]), path), paths)
            '''And then add in the original src/dst if they are hosts'''
            for path in paths:
                if (not JellyRouting.is_switch(src)):
                    path.insert(len(path), dst)
                if (not JellyRouting.is_switch(dst)):
                    path.insert(len(path), dst)
	    self.paths[path_tuple] = paths
	    return choice(self.paths.get(path_tuple))
	return None


class StructuredRouting(Routing):
    '''Route flow through a StructuredTopo and return one path.

    Optionally accepts a function to choose among the set of valid paths.  For
    example, this could be based on a random choice, hash value, or
    always-leftmost path (yielding spanning-tree routing).

    Completely stupid!  Think of it as a topology-aware Dijstra's, that either
    extends the frontier until paths are found, or quits when it has looked for
    path all the way up to the core.  It simply enumerates all valid paths and
    chooses one.  Alternately, think of it as a bidrectional DFS.

    This is in no way optimized, and may be the slowest routing engine you've
    ever seen.  Still, it works with both VL2 and FatTree topos, and should
    help to bootstrap hardware testing and policy choices.  
    The main data structures are the path dicts, one each for the src and dst.
    Each path dict has node ids as its keys.  The values are lists of routes,
    where each route records the list of dpids to get from the starting point
    (src or dst) to the key.

    Invariant: the last element in each route must be equal to the key.
    '''

    def __init__(self, topo, path_choice):
        '''Create Routing object.

        @param topo Topo object
        @param path_choice path choice function (see examples below)
        '''
        self.topo = topo
        self.path_choice = path_choice
        self.src_paths = None
        self.dst_paths = None
        self.src_path_layer = None
        self.dst_path_layer = None

    def _extend_reachable(self, frontier_layer):
        '''Extend reachability up, closer to core.

        @param frontier_layer layer we're extending TO, for filtering paths

        @return paths list of complete paths or None if no overlap
            invariant: path starts with src, ends in dst

        If extending the reachability frontier up yields a path to a node which
        already has some other path, then add that to a list to return of valid
        path choices.  If multiple paths lead to the newly-reached node, then
        add a path for every possible combination.  For this reason, beware
        exponential path explosion.

        Modifies most internal data structures as a side effect.
        '''

        complete_paths = [] # List of complete dpid routes

        # expand src frontier if it's below the dst
        if self.src_path_layer > frontier_layer:

            src_paths_next = {}
            # expand src frontier up
            for node in sorted(self.src_paths):

                src_path_list = self.src_paths[node]
                lg.info("src path list for node %s is %s" %
                        (node, src_path_list))
                if not src_path_list or len(src_path_list) == 0:
                    continue
                last = src_path_list[0][-1] # Last element on first list

                up_edges = self.topo.up_edges(last)
                if not up_edges:
                    continue
                assert up_edges
                up_nodes = self.topo.up_nodes(last)
                if not up_nodes:
                    continue
                assert up_nodes

                for edge in sorted(up_edges):
                    a, b = edge
                    assert a == last
                    assert b in up_nodes
                    frontier_node = b
                    # add path if it connects the src and dst
                    if frontier_node in self.dst_paths:
                        dst_path_list = self.dst_paths[frontier_node]
                        lg.info('self.dst_paths[frontier_node] = %s' %
                                self.dst_paths[frontier_node])
                        for dst_path in dst_path_list:
                            dst_path_rev = copy(dst_path)
                            dst_path_rev.reverse()
                            for src_path in src_path_list:
                                new_path = src_path + dst_path_rev
                                lg.info('adding path: %s' % new_path)
                                complete_paths.append(new_path)
                    else:
                        if frontier_node not in src_paths_next:
                            src_paths_next[frontier_node] = []
                        for src_path in src_path_list:
                            extended_path = src_path + [frontier_node]
                            src_paths_next[frontier_node].append(extended_path)
                            lg.info("adding to self.paths[%s] %s: " % \
                                      (frontier_node, extended_path))

            # filter paths to only those in the most recently seen layer
            lg.info("src_paths_next: %s" % src_paths_next)
            self.src_paths = src_paths_next
            self.src_path_layer -= 1

        # expand dst frontier if it's below the rc
        if self.dst_path_layer > frontier_layer:

            dst_paths_next = {}
            # expand src frontier up
            for node in self.dst_paths:

                dst_path_list = self.dst_paths[node]
                lg.info("dst path list for node %s is %s" %
                        (node, dst_path_list))
                last = dst_path_list[0][-1] # last element on first list

                up_edges = self.topo.up_edges(last)
                if not up_edges:
                    continue
                assert up_edges
                up_nodes = self.topo.up_nodes(last)
                if not up_nodes:
                    continue
                assert up_nodes
                lg.info("up_edges = %s" % sorted(up_edges))
                for edge in sorted(up_edges):
                    a, b = edge
                    assert a == last
                    assert b in up_nodes
                    frontier_node = b
                    # add path if it connects the src and dst
                    if frontier_node in self.src_paths:
                        src_path_list = self.src_paths[frontier_node]
                        lg.info('self.src_paths[frontier_node] = %s' %
                                self.src_paths[frontier_node])
                        for src_path in src_path_list:
                            for dst_path in dst_path_list:
                                dst_path_rev = copy(dst_path)
                                dst_path_rev.reverse()
                                new_path = src_path + dst_path_rev
                                lg.info('adding path: %s' % new_path)
                                complete_paths.append(new_path)

                    else:
                        if frontier_node not in dst_paths_next:
                            dst_paths_next[frontier_node] = []
                        for dst_path in dst_path_list:
                            extended_path = dst_path + [frontier_node]
                            dst_paths_next[frontier_node].append(extended_path)
                            lg.info("adding to self.paths[%s] %s: " % \
                                      (frontier_node, extended_path))

            # filter paths to only those in the most recently seen layer
            lg.info("dst_paths_next: %s" % dst_paths_next)
            self.dst_paths = dst_paths_next
            self.dst_path_layer -= 1

        lg.info("complete paths = %s" % complete_paths)
        return complete_paths

    def get_route(self, src, dst):

        print "GET ROUTE src: " + str(src) + " dst: " + str(dst)
        '''Return flow path.

        @param src source dpid (for host or switch)
        @param dst destination dpid (for host or switch)

        @return flow_path list of DPIDs to traverse (including inputs), or None
        '''

        if src == dst:
          return [src]

        self.src_paths = {src: [[src]]}
        self.dst_paths = {dst: [[dst]]}
	#print "SRC PATHS", self.src_paths
	#print "DST PATHS", self.dst_paths

        src_layer = self.topo.layer(src)
        dst_layer = self.topo.layer(dst)

        # print "src layer: " + str(src_layer)
        # print "dst layer: " + str(dst_layer)

        # use later in extend_reachable
        self.src_path_layer = src_layer
        self.dst_path_layer = dst_layer

        # the lowest layer is the one closest to hosts, with the highest value
        lowest_starting_layer = src_layer
        if dst_layer > src_layer:
            lowest_starting_layer = dst_layer

        # printf "lowest starting layer: " + lowest_starting_layer

        for depth in range(lowest_starting_layer - 1, -1, -1):
	    # print "Depth: " + str(depth)
            lg.info('-------------------------------------------')
            paths_found = self._extend_reachable(depth)
            # print "paths found" + str(paths_found)
            if paths_found:
                path_choice = self.path_choice(paths_found, src, dst)
                lg.info('path_choice = %s' % path_choice)
                return path_choice
        return None

# Disable unused argument warnings in the classes below
# pylint: disable-msg=W0613


class STStructuredRouting(StructuredRouting):
    '''Spanning Tree Structured Routing.'''

    def __init__(self, topo):
        '''Create StructuredRouting object.

        @param topo Topo object
        '''

        def choose_leftmost(paths, src, dst):
            '''Choose leftmost path

            @param path paths of dpids generated by a routing engine
            @param src src dpid (unused)
            @param dst dst dpid (unused)
            '''
            return paths[0]

        super(STStructuredRouting, self).__init__(topo, choose_leftmost)


class RandomStructuredRouting(StructuredRouting):
    '''Random Structured Routing.'''

    def __init__(self, topo):
        '''Create StructuredRouting object.

        @param topo Topo object
        '''

        def choose_random(paths, src, dst):
            '''Choose random path

            @param path paths of dpids generated by a routing engine
            @param src src dpid (unused)
            @param dst dst dpid (unused)
            '''
            return choice(paths)

        super(RandomStructuredRouting, self).__init__(topo, choose_random)


class HashedStructuredRouting(StructuredRouting):
    '''Hashed Structured Routing.'''

    def __init__(self, topo):
        '''Create StructuredRouting object.

        @param topo Topo object
        '''

        def choose_hashed(paths, src, dst):
            '''Choose consistent hashed path

            @param path paths of dpids generated by a routing engine
            @param src src dpid
            @param dst dst dpid
            '''
            src_dpid = self.topo.id_gen(name = src).dpid
            dst_dpid = self.topo.id_gen(name = dst).dpid
            hash_ = crc32(pack('QQ', src_dpid, dst_dpid))
            path = paths[hash_ % len(paths)]
            return path

        super(HashedStructuredRouting, self).__init__(topo, choose_hashed)
# pylint: enable-msg=W0613
